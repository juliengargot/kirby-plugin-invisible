# Kirby *Invisible* plugin

In the context of Kirby, the plugin simply allows you to redirect non visible pages to the default Kirby error page for non-login users.

## Requirement and Settings

None. Ready to go when installed.

## Installation

Download or clone the repository in `/site/plugins`.

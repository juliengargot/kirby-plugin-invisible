<?php
if( site()->multilang() ) {
  // If site is multilang.
  $languages = array_keys( site()->languages()->toArray() );
  $codes = implode("|", $languages);
  $pattern = '(?:('. $codes .')/?)?(:all)';
}
else {
  // If site has zero or one lang.
  $pattern = '(?:('.site()->language().')/?)?(:all)';
}

kirby()->routes(array(
  array(
    'pattern' => $pattern,
    'action'  => function($lang, $uri = null) {

      if( $uri == '' || $uri == '/') {
        $uri = 'home';
      }

      $page = page($uri);

      // If page invisible and user is not connected, go error.
      if (!$page || ($page->isInvisible() && !site()->user()) ) {
        $page = site()->errorpage();
      }

      return site()->visit($page, $lang);
    }
  )
));
